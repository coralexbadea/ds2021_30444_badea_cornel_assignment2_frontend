# stage 1
FROM node:14-alpine as node

# Create app directory
WORKDIR /usr/src/app

# Bundle app source (make sure you have package.json copied)
COPY . .

# install dependencies, @angular cli will be installed here as well
RUN npm install

# build your project into dist folder
RUN npm run build

# stage 2
FROM nginx:alpine
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'

WORKDIR /usr/share/nginx/html
COPY --from=node /usr/src/app/dist/project-ds .

