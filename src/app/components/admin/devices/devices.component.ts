import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Device } from 'src/app/models/Device';
import { User } from 'src/app/models/User';
import { DeviceService } from 'src/app/services/device.service';
import { UserService } from 'src/app/services/user.service';
import * as fromUserState from 'src/app/store/user/user.selectors';
@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss','../admin.component.scss']
})
export class DevicesComponent implements OnInit {

  public message: string = ""
  public newDevice: Device = new Device()
  public devices: Device[] = []
  public currentUserId: number = -1;
  constructor( private route: ActivatedRoute,
      private deviceService: DeviceService) { }

  ngOnInit(): void {
    this.getClient()
  }

  getClient(){
    const id = Number(this.route.snapshot.paramMap.get('id'))
    this.currentUserId = id;
    this.deviceService.getDevices(id).subscribe(result=>{
      this.devices = result
    })
  }

  deleteDevice(id:number){
    console.log(id)
    this.deviceService.deleteDevice(id).subscribe(result=>console.log(result.body))
    this.devices = this.devices.filter(device=>
      device.deviceId !== id
    )
  }

  editDevice(device: Device){
    console.log(device)
    this.deviceService.updateDevice(device).subscribe(result=>console.log(result))
    this.devices = this.devices.map((u)=>{
      if(u.deviceId==device.deviceId){
        return device
      }
      else{
        return u
      }
    })
  }

  addDevice(){
    if(this.newDevice.maxEnCons && this.newDevice.sensorMaxVal && this.newDevice.maxEnCons > this.newDevice?.sensorMaxVal){
      this.message = "My man Max. En. Cons. of Device must be less than sensor Max. Value"
      return false
    }
    if(!this.newDevice.deviceName || !this.newDevice.location || !this.newDevice.sensorDescription){
        this.message = "Please no empty fields"
        return false
    }

    console.log(this.newDevice)
    this.newDevice.userId = this.currentUserId;
    this.message = ""

    this.deviceService.addDevice(this.newDevice).subscribe(
      data => {
        this.deviceService.getDevices(this.currentUserId).subscribe(result=>{
          this.devices = result
        })
      },
      err => this.message = "An Error Occured"
    )
  
    return true
  
  }



}
