import { Component, Inject, OnDestroy, OnInit} from '@angular/core';
import { WebSocketAPI } from '../../websocket/websocket';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { WebSocketPayload } from 'src/app/models/WebSocketPaylod';
import * as fromUserState from 'src/app/store/user/user.selectors';
import { Store } from '@ngrx/store';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit, OnDestroy{

  public webSocketAPI: WebSocketAPI | undefined;
  public message: string = ""
  public show_popup : boolean = true;
  private user : User | null = null;
  private userStoreSubscription: any;
  constructor(private store:Store<any>,
    private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.webSocketAPI = new WebSocketAPI(new PopupComponent(this.store, this.dialog));
    this.connect()
  }

  ngOnDestroy(): void {
    this.userStoreSubscription?.unsubscribe();  
    this.webSocketAPI?._disconnect();
  }

  connect(){
    this.webSocketAPI?._connect();
  }

  disconnect(){
    this.webSocketAPI?._disconnect();
  }

  sendMessage(){
    this.webSocketAPI?._send("kappa");
  }

  handleMessage(payload: WebSocketPayload){
    this.userStoreSubscription = this.store.select(fromUserState.getUserState).subscribe(user => {
      this.user = user
    })

    if(this.user?.userId === payload.userId){
      this.message = payload?.message
      this.openDialog()
    }
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '500px',
      data: {message: this.message}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}


export interface DialogData {
  message: string;
}


@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: './dialog.html',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
