import { HttpClient, HttpHandler, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs-compat';
import { Device } from 'src/app/models/Device';
import { User } from 'src/app/models/User';
import { ChartService } from 'src/app/services/chart.service';
import { DeviceService } from 'src/app/services/device.service';
import * as fromUserState from 'src/app/store/user/user.selectors';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],

})
export class HomeComponent implements OnInit {

  private subscriptions: Subscription[] = [];
  public user = new User();
  public devices : Device[] = [];
  public selectedDevice: number = -1;

  public date = new FormControl(new Date());
 
  public stringSelectedDevices = "Select Device"
  constructor(private store: Store<any>,
    private deviceService: DeviceService,
    private chartService : ChartService) { }

  ngOnInit(): void {
    this.chartService.setDeviceId(this.selectedDevice);
    this.chartService.setDate(this.date.value?this.date.value:(new Date()))


    this.subscriptions.push(this.store.select(fromUserState.getUserState).subscribe(user => {
      console.log(user);
      if(user){
        this.user = user;
        this.deviceService.getDevices(this.user.userId).subscribe(devices =>
          {
            if(devices.length==0){this.stringSelectedDevices="No devices found"}
            this.devices=devices
          })
      }
    }))
  }

  onSelect(something:any){
    this.chartService.setDeviceId(this.selectedDevice)
  }

  getDate(date:MatDatepickerInputEvent<Date>){
    this.chartService.setDate(date.value?date.value:(new Date()))
    this.selectedDevice = -1;
    this.chartService.setDeviceId(this.selectedDevice)
  }




}


