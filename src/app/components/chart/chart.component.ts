import { Component, Input, OnInit } from '@angular/core';
import { ChartOptions, ChartType,  ChartDataSets} from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { Measure } from 'src/app/models/Measure';
import { ChartService } from 'src/app/services/chart.service';
import { MeasureService } from 'src/app/services/measure.service';


@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  public deviceId: number = -1;
  private date : Date = new Date();
  
  private measuresOneDay: Measure[] | null= [];
  private valuesOneDay : number[] =[];
  public lineChartData: ChartDataSets[] = [
    { data:  this.valuesOneDay, label: 'kWh' },
  ];
  
  public lineChartLabels: Label[] = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"];
    
  public lineChartOptions = {
    responsive: true,
  };
     
  public lineChartLegend = true;
  public lineChartType : ChartType = 'line'
  public lineChartPlugins = [];
    

  constructor(private measureService : MeasureService,
    private chartService : ChartService) { 
      
  }
   
  ngOnInit() {

    this.chartService.getSubjectDeviceId().subscribe(id => {
        this.deviceId=id
        this.getMeasureSubscription();
      })

      this.chartService.getSubjectDate().subscribe(date => {
        this.date = date
        this.getMeasureSubscription();
      })
  }

  getMeasureSubscription(){
    return this.measureService.getMeasuresOneDay(this.deviceId , this.date).subscribe(response => {
      this.measuresOneDay = response.body
      console.log(this.measuresOneDay)
      if(this.measuresOneDay != null){
        this.valuesOneDay = this.getValuesFromMeasures(this.measuresOneDay)

        this.lineChartData = [
          { data:  this.valuesOneDay, label: 'kWh' },
        ];
        console.log(this.valuesOneDay)
      }

    })
  }

  getValuesFromMeasures(measures : Measure[]) : number[]{
    const average = (arr: number[]) => arr.reduce((a,b) => a + b, 0) / arr.length;
    var result : number[] = [];
    
    for(let i=0; i <= 24; i++){
      var avgHour = average(measures.filter((m) => {
        if(m.hour)
          return (m.hour >= i && m.hour < (i+1))
        else
          return false;
      }).map((m)=>m.enCons?m.enCons:0))
      result.push(avgHour?avgHour:0)
    }
   
    return result
  }




}
