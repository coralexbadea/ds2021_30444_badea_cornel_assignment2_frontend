import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Store, } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Login } from 'src/app/store/user/user.actions';
import * as fromUserState from '../../store/user/user.selectors';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit{

  private user : User | null = new User();
  private loginAttempt = false;
  private subscriptions: Subscription[] = [];
  public loginErrorsMessage:string | undefined;
  public url:string="home"

  loginForm = this.fb.group({
    userName: ['',Validators.required],
    password: ['', Validators.required]
  });


  constructor(private fb: FormBuilder, private store: Store<any>,private router:Router) {
    
  }

  ngOnInit(){
    
      this.subscriptions.push(this.store.select(fromUserState.getUserState).subscribe(user => {
        this.user = user},
      ))
      
      
      this.store.select(fromUserState.getLoginErrorMessage).subscribe(
        data => this.loginErrorsMessage = data
        );
  };

  onSubmit():void {

    this.loginAttempt = true;
    this.loginErrorsMessage = "#";

    if(this.getEmailInputErrors() || this.getPasswordInputErrors()){
      return ;
    }

    const payload = {
      "userName" : this.loginForm.value.userName,
      "password" : this.loginForm.value.password
    };
    this.store.dispatch(new Login(payload));
  }

  getEmailInputErrors(){
    return !this.loginForm.controls['userName'].valid  && this.loginAttempt;
  }

  getPasswordInputErrors(){
    return !this.loginForm.controls['password'].valid  && this.loginAttempt;
  }


  getInputErrors(){
    return this.getEmailInputErrors() || this.getPasswordInputErrors();
  }

  navigateToHome(){
    if(this.loginErrorsMessage == ""){
      var result = this.user?.roles?.indexOf("ADMIN") 
      if(result!=undefined && result != -1){
        this.router.navigate(['/admin'])
      }
      else{
        this.router.navigate(['/home'])
      }

    }
  }

  getLoginErrors(){
    this.navigateToHome();
    return this.loginErrorsMessage !== "" && this.loginErrorsMessage !== "#" ? true : false;
  }

  getLoginAttempt(){
    return this.loginAttempt;

  }

  hideLoginErrors(){
    this.loginAttempt = false;
  }
  
}