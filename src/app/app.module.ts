import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LoginEffects } from './store/user/user.effects';
import { FEATURE_NAME, reducers } from './store/app.states';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { MatDialogModule } from '@angular/material/dialog';

import { AppComponent } from './app.component';
import { RegisterViewComponent } from './components/register-view/register-view.component';
import { ClientItemComponent } from "./components/admin/client-item/client-item.component";
import { DeviceItemComponent } from "./components/admin/device-item/device-item.component";
import { DevicesComponent } from "./components/admin/devices/devices.component";
import { AdminComponent } from "./components/admin/admin.component";
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';

import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { SpringbootInterceptor } from './springboot.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from '@angular/material/select';
import { CalculateAgePipe } from './pipes/calculate-age.pipe';
import { ChartComponent } from './components/chart/chart.component';
import { ChartsModule } from 'ng2-charts';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { PopupComponent } from './components/popup/popup.component';

@NgModule({
  declarations: [
    AppComponent,
  
    RegisterViewComponent,
    LoginComponent,
    HomeComponent,
    CalculateAgePipe,
    ChartComponent,
    ClientItemComponent,
    DeviceItemComponent,
    DevicesComponent,
    AdminComponent,
    PopupComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, {}),
    EffectsModule.forRoot([LoginEffects]),
    StoreModule.forFeature(FEATURE_NAME, reducers),
    FormsModule, ReactiveFormsModule,
    FontAwesomeModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSelectModule,
    ChartsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule
      
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
      useClass: SpringbootInterceptor,
      multi: true
    },
    MatDatepickerModule 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
