import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterViewComponent } from './components/register-view/register-view.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AdminComponent } from './components/admin/admin.component';
import { DevicesComponent } from './components/admin/devices/devices.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuardService],  data: {role:"CLIENT"}},
  {path: 'admin', component: AdminComponent ,canActivate: [AuthGuardService], data: {role:"ADMIN"}},
  {path: 'register', component: RegisterViewComponent},
  {path:'admin/:id', component: DevicesComponent},

  {path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
