export interface WebSocketPayload{
    userId: number;
    message:string;
}