export class User{
    userId?: number;
    userName?:string;
    password?:string;
    name?:string;
    bdate?:string;
    address?:string;
    roles?:string[];
    token?:string
}