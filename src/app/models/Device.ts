export class Device{
    deviceId?:number;
    deviceName?:String;
    location?:String;
    maxEnCons?:number;
    avgEnCons?:number;
    userId?: number;

    sensorDescription?:String;
    sensorMaxVal?:number;

}