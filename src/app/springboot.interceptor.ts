import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import * as fromUserState from 'src/app/store/user/user.selectors';
import { Store } from '@ngrx/store';
import { map, switchMap } from 'rxjs/operators';

@Injectable()
export class SpringbootInterceptor implements HttpInterceptor {

  constructor(private store: Store<any>) {
   

  }

  
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    let httpHeaders = new HttpHeaders({
      'Cache-Control': 'no-cache',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
      //'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials' : 'true',
      // 'Access-Control-Expose-Headers': ['Set-Cookie','Cookie']
      //  'Authorization': `Bearer ${this.store.select(fromUserState.getUserState).pipe(map(user=> user?.token))}`,
    });

    if (request.headers.get("skip") == null){
      return this.store.select(fromUserState.getUserState).pipe(switchMap(user => {
        httpHeaders = httpHeaders.append('Authorization', `Bearer ${ user?.token}`);
        const clonedRequest = request.clone({headers: httpHeaders, 
          // withCredentials:true, 
        });
        return next.handle(clonedRequest);
      }))
      
    }
    else{
      const clonedRequest = request.clone({headers: httpHeaders, 
        // withCredentials:true, 
      });
      return next.handle(clonedRequest);
    }

  }
}
