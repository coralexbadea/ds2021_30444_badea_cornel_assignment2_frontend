import { ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import { userReducer, UserState } from './user/user.reducers';


export const FEATURE_NAME = "officePlanner";
export const selectOfficePlanner = createFeatureSelector<AppState, OfficePlannerState>(FEATURE_NAME);

export interface OfficePlannerState{
    currentUserState: UserState;
}

export interface AppState{
    officePlanner:OfficePlannerState
}

export const reducers:ActionReducerMap<OfficePlannerState, any> = {
    currentUserState: userReducer,
};


