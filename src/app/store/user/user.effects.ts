import { Injectable } from "@angular/core";
import { Actions, createEffect, Effect} from '@ngrx/effects';
import { Router } from "@angular/router";
import { LoginService } from "src/app/services/login.service";
import { Observable, of } from "rxjs";
import { AuthActionTypes, Login, LoginSuccess, LoginFailed,Logout } from "./user.actions";

import { tap, map, switchMap, catchError } from 'rxjs/operators';
import { ofType } from "@ngrx/effects";
import { User } from "src/app/models/User";


@Injectable()
export class LoginEffects{
    constructor(
        private actions: Actions,
        private loginService: LoginService,
        private router: Router
    ){}


    Login$ = createEffect(() => this.actions.pipe(
        ofType(AuthActionTypes.LOGIN),
        map((action: Login) => action.payload),
        switchMap(payload => {
            return this.loginService.login(payload)
            .pipe(
                map((user:User|null) => { 
                    return new LoginSuccess(user);
                 }),
                 catchError(error => of(new LoginFailed(error)))
            )
        })
    ));

}