import { ActionReducerMap, on } from "@ngrx/store";
import { Action } from "rxjs/internal/scheduler/Action";
import { User } from "src/app/models/User";
import { AuthActionTypes, All } from './user.actions';

export interface UserState{
    isLoggedin: boolean;
    user:User | null;
    errorMessage: string;
}

export const initialState: UserState = {
    isLoggedin: false,
    user: new User(),
    errorMessage: ""
};

export function userReducer(state:UserState = initialState, action: All):UserState{
    switch(action.type){
        case AuthActionTypes.LOGIN:{
            return {
                ...state,
                isLoggedin: false,
                user:null,
                errorMessage: "#"
            };
        }
        case AuthActionTypes.LOGIN_SUCCESS: {
            return {
                ...state,
                isLoggedin: true,
                user:action.payload,
                errorMessage: ""
            };
        }
        case AuthActionTypes.LOGIN_FAILED: {
            
            return {
                ...state,
                isLoggedin: false,
                user:null,
                errorMessage: action.payload["error"]
            }
        }

        case AuthActionTypes.LOGGED_OUT:{
            return {
                ...state,
                isLoggedin:false,
                user:null,
                errorMessage:"-"
            }
        }

        default: 
            return state;
        
    }
}