import { createSelector } from "@ngrx/store";
import { OfficePlannerState, selectOfficePlanner} from "../app.states";
import { JwtHelperService } from '@auth0/angular-jwt';

export const selectCurrentUserState = createSelector(
    selectOfficePlanner,
    (state: OfficePlannerState) => {
      return state.currentUserState;
    }
  );

export const getUserState = createSelector(
    selectCurrentUserState,
    state => {
      return state.user;
    }
  );

export const getLoginState = createSelector(
  selectCurrentUserState,
  state => {
    const jwtHelper:JwtHelperService = new JwtHelperService()
    return state.isLoggedin && !jwtHelper.isTokenExpired(state.user?.token)
  }
);

export const getLoginErrorMessage = createSelector(
  selectCurrentUserState,
  state => {
    return state.errorMessage;
  }
);
