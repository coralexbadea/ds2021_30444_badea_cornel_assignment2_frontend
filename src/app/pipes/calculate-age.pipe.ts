import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'calculateAge'
})
export class CalculateAgePipe implements PipeTransform {

  transform(bdate: string | undefined, ...args: unknown[]): any {

    var today = new Date();
    if(bdate){
      var birthDate = new Date(bdate?bdate:"");
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      return age.toString()
    }
    return "Can not determine age";

  }

}
