import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { ChartComponent } from '../components/chart/chart.component';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Measure } from '../models/Measure';
import { API } from '../services/constants';
import { PopupComponent } from '../components/popup/popup.component';

export class WebSocketAPI {
    private BASE_URL=`http://localhost:8080/prefix/api/measure`;


    webSocketEndPoint: string = 'https://ds2021-cornel-badea.herokuapp.com/ws';
    topic: string = "/topic/greetings";
    stompClient: any;
    appComponent: PopupComponent;
    constructor(appComponent: PopupComponent){
        this.appComponent = appComponent;
    }
    _connect() {
        console.log("Initialize WebSocket Connection");
        let ws = new SockJS(this.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        const _this = this;
        _this.stompClient.connect({}, function () {
            _this.stompClient.subscribe(_this.topic, function (sdkEvent: any) {
                _this.onMessageReceived(sdkEvent);
            });
            //_this.stompClient.reconnect_delay = 2000;
        }, this.errorCallBack);
    };

    _disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log("Disconnected");
    }

    // on error, schedule a reconnection attempt
    errorCallBack(error: string) {
        console.log("errorCallBack -> " + error)
        setTimeout(() => {
            this._connect();
        }, 5000);
    }

 /**
  * Send message to sever via web socket
  * @param {*} message 
  */
    _send(message: string) {
        console.log(`web socket to send a message ${message}`);
    }

    onMessageReceived(message: any) {
        console.log("Message Recieved from Server :: " + JSON.stringify(JSON.parse(message.body)));
        this.appComponent.handleMessage(JSON.parse(message.body));
    }
    
}