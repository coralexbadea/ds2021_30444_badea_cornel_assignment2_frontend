import { Injectable } from '@angular/core';
import { User } from 'src/app/models/User';
import { HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { API } from './constants';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private BASE_URL = `${API}/register`

  constructor(private http : HttpClient) { }

  register(user: User){
    return this.http.post(this.BASE_URL,user, {...Option, headers:{skip:"true"},responseType:'text'}).pipe(catchError(this.handleError));
  }

  handleError(error:any){
    if(error.status == 0)
      console.error('Error:',error.error)
    else{
      console.error('Error, backend returned:',error.error);
    }
    return throwError(
      error.error
    )
  }
}
