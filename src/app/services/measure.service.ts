import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Measure } from '../models/Measure';
import { API } from './constants';
@Injectable({
  providedIn: 'root'
})
export class MeasureService {

  private BASE_URL=`${API}/measure`;

  private httpHeaders = new HttpHeaders({
    'Cache-Control': 'no-cache',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
    // 'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials' : 'true',
    'Authorization': `Bearer ${localStorage.getItem("token")}`,
  });

  constructor(private http:HttpClient) { }

  getMeasuresOneDay(deviceId: number, date : Date):Observable<HttpResponse<Measure[]>>{
    date.setHours(0)
    console.log(date.getTime())
    return this.http.post<Measure[]>(`${this.BASE_URL}/forDeviceOneDay/${deviceId}`,
                                      {"timestamp":date.getTime()}, {...Option, headers:this.httpHeaders,  observe:'response' });
  }

  
}
