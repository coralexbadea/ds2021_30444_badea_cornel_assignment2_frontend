import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  private deviceId: number = -1;
  private subject = new Subject<any>();

  private date: Date = new Date();
  private subject1 = new Subject<any>();


  constructor() { }

  setDeviceId(newId:number){
    this.deviceId = newId
    this.subject.next(this.deviceId)
  }

  getSubjectDeviceId():Observable<any>{
    return this.subject.asObservable()
  }

  setDate(newDate:Date){
    this.date = newDate
    this.subject1.next(this.date)
  }

  getSubjectDate():Observable<any>{
    return this.subject1.asObservable()
  }

}
