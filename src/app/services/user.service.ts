import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { observable } from 'rxjs';
import { Observable } from 'rxjs-compat';
import { User } from '../models/User';
import { API } from './constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private BASE_URL=`${API}/user`;


  constructor(private http:HttpClient) { 
    
  }

  getUsers():Observable<User[]>{
    return (this.http.get<User[]>(`${this.BASE_URL}`));
  }

  deleteUser(id:number){
    return this.http.delete(`${this.BASE_URL}/${id}`, {...Option, responseType: 'text', observe:'response' })
  }

  updateUser(user:User){
    return this.http.put(`${this.BASE_URL}/${user.userId}`,user,{...Option, observe:'response' } )
  }
}
